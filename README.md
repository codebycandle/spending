# README #

SPENDING is an experiment in D3 (JS), and serves as a parser for CSV records obtained from a user's MINT.COM account.  This tool's objective is to help illustrate personal monthly spending totals via "quick-loading" chart display.  (A previous incarnation of this was started in C# / Unity, but the splash screen load times led to a sluggish user experience.)

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact